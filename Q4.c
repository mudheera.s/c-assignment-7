#include <stdio.h>
#include <string.h>
#define N 2

int main(){
    int m1[N][N]={{2,3},{3,4}};
    int m2[N][N]={{2,3},{3,4}};
    int m3[N][N];
    int sum;

    for(int i=0;i<N;i++){
        for(int j=0;j<N;j++){
            sum=0;
            for(int k=0; k<N; k++){
                sum = sum + m1[i][k] * m2[k][j];
                m3[i][j] = sum;
            }
        }
    }
    for(int i=0;i<N;i++){
        for(int j=0;j<N;j++){
            printf("%d\t",m3[i][j]);
        }
        printf("\n");
    }
}
