#include <stdio.h>
#include <string.h>

int main(){
    char array[255];
    char character;
    int count=0;

    printf("Enter a sentence: ");
    fgets(array,sizeof(array),stdin);

    printf("Enter a character you need to search: ");
    scanf("%c",&character);

    for(int i=0;i<strlen(array);i++){
        if(array[i]==character){
            printf("The frequency is %d\n",i+1);
            count++;
        }
        if(count==0){
            printf("There isn't this character in the sentence");
        }
    }
}
