#include <stdio.h>
#include <string.h>
#define N 2

int main(){
    int m1[N][N]={{2,3},{3,4}};
    int m2[N][N]={{2,3},{3,4}};
    int m3[N][N];

    for(int i=0;i<N;i++){
        for(int j=0;j<N;j++){
            m3[i][j]=m1[i][j]+m2[i][j];
        }
    }
    for(int i=0;i<N;i++){
        for(int j=0;j<N;j++){
            printf("%d\t",m3[i][j]);
        }
        printf("\n");
    }
}
